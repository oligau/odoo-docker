#!/bin/bash
# script took from https://linuxize.com/post/how-to-setup-automatic-odoo-backup/

## Make script executable
# chmod +x ~/backup_odoo.sh

## Schedule job
# crontab -e
# 30 1 * * * /home/<yourusername>/backup_odoo.sh

## Manual restore
# http://localhost:8069/web/database/manager

## Restore using CLI
# curl -F 'master_pwd=superadmin_passwd' \
#    -F backup_file=@/home/<yourusername>/db1.2018-04-14.zip \
#    -F 'copy=true' \
#    -F 'name=db3' \
#    http://localhost:8069/web/database/restore

# vars
BACKUP_DIR=~/odoo_backups
ODOO_DATABASE=db1
ADMIN_PASSWORD=superadmin_passwd

# create a backup directory
mkdir -p ${BACKUP_DIR}

# create a backup
curl -X POST \
    -F "master_pwd=${ADMIN_PASSWORD}" \
    -F "name=${ODOO_DATABASE}" \
    -F "backup_format=zip" \
    -o ${BACKUP_DIR}/${ODOO_DATABASE}.$(date +%F).zip \
    http://localhost:8069/web/database/backup


# delete old backups
find ${BACKUP_DIR} -type f -mtime +7 -name "${ODOO_DATABASE}.*.zip" -delete
