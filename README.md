About this Repo
======

This is the Git repo of the official Docker image for [Odoo](https://registry.hub.docker.com/_/odoo/). See the Hub page for the full readme on how to use the Docker image and for information regarding contributing and issues.

The full readme is generated over in [docker-library/docs](https://github.com/docker-library/docs), specifically in [docker-library/docs/odoo](https://github.com/docker-library/docs/tree/master/odoo).

## Build and push image

```shell
docker logout registry.gitlab.com
docker login registry.gitlab.com

export ODOO_BRANCH="saas-16.3"
export ODOO_COMMIT="c79fe39"
export ODOO_DATE="20240128"
export REGISTRY="registry.gitlab.com/oligau/odoo-docker"

docker buildx build \
  --no-cache \
  --progress plain \
  --push \
  --provenance false \
  -t ${REGISTRY}/odoo:${ODOO_BRANCH}-${ODOO_DATE} \
  -f saas-16.3/Dockerfile \
  --build-arg "ODOO_BRANCH=${ODOO_BRANCH}" \
  --build-arg "ODOO_COMMIT=${ODOO_COMMIT}" \
.
```

## Tag latest build

```shell
docker pull $REGISTRY/odoo:${ODOO_BRANCH}-${ODOO_DATE}
docker tag \
  $REGISTRY/odoo:${ODOO_BRANCH}-${ODOO_DATE} \
  $REGISTRY/odoo:latest
```

## Start

```shell
export REGISTRY="registry.gitlab.com/oligau/odoo-docker"
docker run \
    -d \
    -v odoo-db:/var/lib/postgresql/data \
    -e POSTGRES_USER=odoo \
    -e POSTGRES_PASSWORD=odoo \
    -e POSTGRES_DB=postgres \
    --name odoo_db \
    postgres:15

docker run \
    -d \
    -v odoo-data:/var/lib/odoo \
    -p 8069:8069 \
    --name odoo \
    --link db:db \
    -t ${REGISTRY}/odoo:latest
```

## Deploy using Docker Compose

Clone repository.

```shell
sudo apt install git openssl
git clone https://gitlab.com/oligau/odoo-docker.git
cd odoo-docker
```

Generate a postgresql password file.

```shell
mkdir -p config
openssl rand -hex 32 > config/odoo_pg_pass
```

Configure master password and backup script.

```shell
cp saas-16.3/odoo.conf config/odoo.conf
ADMIN_PASSWD=$(openssl rand -hex 4)
echo "admin_passwd = ${ADMIN_PASSWD}" >> config/odoo.conf
DATABASE_NAME=my-database1
sed -i.bak "s/db1$/${DATABASE_NAME}/" scripts/backup_odoo.sh
sed -i "s/superadmin_passwd$/${ADMIN_PASSWD}/" scripts/backup_odoo.sh
```

Schedule backup job at 1:30 AM everyday.

```shell
JOB="30 1 * * * ${PWD}/scripts/backup_odoo.sh"
((crontab -l 2>/dev/null || true; echo "$JOB") | sort -u) | crontab -
```

Start Odoo.

```shell
ODOO_TAG=saas-16.3-20240128 docker compose up -d
```

Consult logs.

```shell
docker logs -f odoo-docker-odoo-1
```

Open Odoo http://localhost:8069/web/database/manager

## Uninstall enterprise modules from database

Enter Odoo shell.

```shell
DATABASE_NAME=my-database1
docker compose exec odoo python3 odoo-bin shell --db_host=db --db_user=odoo --db_password=$(cat config/odoo_pg_pass) -d ${DATABASE_NAME}
```

Uninstall enterprise modules.

```
self.env['ir.module.module'].search([('name', '=', 'account_invoice_extract')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'analytic_enterprise')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'currency_rate_live')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'digest_enterprise')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'iap_extract')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'l10n_ca_check_printing')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'mail_enterprise')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'mail_mobile')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'pos_enterprise')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'spreadsheet_dashboard_edition')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'spreadsheet_dashboard_stock')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'spreadsheet_edition')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'stock_account_enterprise')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'stock_enterprise')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'web_cohort')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'web_enterprise')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'web_gantt')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'web_grid')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'web_map')]).button_immediate_uninstall()
self.env['ir.module.module'].search([('name', '=', 'web_mobile')]).button_immediate_uninstall()
quit()
```
